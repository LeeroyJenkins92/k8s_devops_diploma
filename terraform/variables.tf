variable "yc_token" {
  description = "yc_token"
  type        = string
  sensitive   = true
}

variable "yc_cloud_id" {
  description = "yc_cloud_id"
  type        = string
  sensitive   = true
}

variable "yc_folder_id" {
  description = "yc_folder_id"
  type        = string
  sensitive   = true
}